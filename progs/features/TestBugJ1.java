class TestBugJ1 {
	public static void main(String[] a) {
		System.out.println(new Test().f());
	}
}

class Test {

	public int f() {
		// sets x as an integer and y as a boolean
		int x;
		boolean y;
		x = 5;
		y = true;
		// tests scenario of adding a boolean to an integer
		x += y;
		// returns value of x at the end of the operation
		return x;
		// this program should cause a type error at compilation,
		// because y is not an integer.

		// however, if a compiler is written incorrectly,
		// it will compile anyway and without an error message
	}
}
