class TestBugJ2 {
	public static void main(String[] a) {

		// should printout 20 if '+=' has been implemented correctly
		// will printout 5 if '+=' is simply assigning y to x
		System.out.println(new Test().f());
	}
}

class Test {

	public int f() {
		// sets x and y as integers
		int x;
		int y;
		x = 15;
		y = 5;
		// tests if '+=' is assinging the result of x+y to x,
		// instead of simply assinging y to x
		x += y;
		// returns value of x at the end of the operation
		return x;
	}
}
