class TestBugJ3 {
	public static void main(String[] a) {
		// prints out 20 if x is being modified
		// prints out 5 if x is not being modified
		System.out.println(new Test().f());
	}
}

class Test {

	public int f() {
		// sets x as an integer of value 5
		int x;
		int y;
		x = 5;
		y = 15;
		// checks if it does modify x at all
		x += y;
		// returns values of x at the end of the operation
		return x;
	}
}
